
from flask import Flask, render_template
from flask_socketio import SocketIO

app = Flask(__name__)
app.config['SECRET_KEY'] = 'vnkdjnfjknfl1232#'
socketio = SocketIO(app)

@app.route('/')
def sessions():
    return render_template('session.html')

def messageReceived(methods=['GET', 'POST']):
    print('message was received!!!')

@socketio.on('my event')
def handle_my_custom_event(json, methods=['GET', 'POST']):
    print('received my event: ' + str(json))
    socketio.emit('my response', json, callback=messageReceived)

@socketio.on('board')
def handle_board_change(json, methods=['GET', 'POST']):
    print('wysłano tablicę')
    socketio.emit('board responce', json)

@socketio.on('join game')
def handle_player_join(json, methods=['GET', 'POST']):
    socketio.emit('player join responce', json)

@socketio.on('clear game')
def handle_clear_game(json, methods=['GET', 'POST']):
    socketio.emit('clear game responce', json)


if __name__ == '__main__':
    socketio.run(app, debug=True)